  @extends('layouts.main')
  @section('content')
<div class="container-fluid">

          <!-- Page Heading -->
          <!-- <h1 class="h3 mb-2 text-gray-800">Tables</h1> -->
          <!-- <p class="mb-4">DataTables<a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Email List</h6>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                <table id="user_table" class="table table-bordered table-striped">
                 <thead>
                  <tr>
                   <th width="35%">Name</th>
                    <th width="35%">Email</th>
                    <th width="30%">Campaign</th>
                  </tr>
                 </thead>
                </table>
              </div>
            </div>
          </div>

        </div>
      @endsection