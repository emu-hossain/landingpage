<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Live-Wallpapers</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link src="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/elements.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <script src="{{asset('js/vendor/modernizr-2.8.3.min.js')}}"></script>
</head>

<body>
    <!-- prelaoder start -->
    <div id="preloader-wrapper">
        <div class="preloader-wave-effect"></div>
    </div>
    <!-- prelaoder end -->
    <!-- page wrapper start -->
    <div id="page-top" class="wrapper">
        <!-- header area start -->
        <header>
            <nav class="navbar navbar-fixed-top topnav" data-spy="affix" data-offset-top="1">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation" aria-expanded="false">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand logobrand" href="#"><img src="{{asset('public/img/logo/logo.png')}}" alt="logo" class="img-responsive"></a>
                    </div>
                    <div class="collapse navbar-collapse" id="navigation">
                        <ul class="nav navbar-nav navbar-right">
                            <!-- <li><a href="#app-about-area">JOIN OUR TEAM</a></li> -->
                            <li><a href="#awesome-features-area">OUR PRODUCTS</a></li>
                          
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <!-- header area end -->
        <!-- slider area start -->
        <section id="slider-area" class="home-style-1 blue-grad-bg">

            <?php 
            if (Session::has('message'))
                {
                     $msg = Session::get('message');  
                     echo $msg;
                    ?>
                      <script type="text/javascript">
                        pop();

                     </script>
                <?php }

             ?>
                        <div class="slider-wrapper">
                <div class="slide">
                    <div class="container">
                        <div class="slider-text ">
                            <h1 data-animation="fadeInDown" data-delay="0.3s" class="eemm">Coming Soon!</h1>
                            <ul class="homeul">
                                <li><span class="li-item">Totally free<span></li>
                                <li><span class="li-item">Lots of mind blowing wallpapers<span></li>
                            </ul>
                            <p data-animation="fadeInDown" data-delay="0.8s" class="eemm">Best Live Wallpapers app in the AppStore. To get it on launch day put your email below.</p>
                            <div class="home-form get-in-touch eemm" >

                                <form id="email_form" action="{{ route('user.email.submit') }}" method="POST" name="appai_message_form">
                                     @csrf
                                            <input type="hidden" name="purpose" id="purpose" value="wallpaper">
                                            <input type="hidden" name="campname" id="campname" value="colorapp">
                                           
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control" id="name" placeholder="Your Name*" required="">
                                            <div class="form-grad-border"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Email Address*" required="">
                                            <div class="form-grad-border"></div>
                                        </div>
                                        <div class="form-group">
                                            <input type="submit" class="btn apple" id="submit"  value="Submit">
                                            <div class="form-grad-border"></div>
                                        </div>
                                        
                                        <p class="appai-form-send-message"></p>
                                    </form>
                            </div>
                            
                        </div>
                        <!-- <div class="header-anim__device"> -->
                            <div class="header-anim__video">
                                <video autoplay="" class="vd" loop="" muted="" poster="/apps/6fb899d7c71275bc16da2675927f3876/images/back_video_top.png" preload="auto">
                                    @if (isset($vid))
                                    <source src="{{asset('public/videos/')}}/{{$vid}}" type="video/webm">
                                    <source  src="{{asset('public/videos/')}}/{{$vid}}" type="video/mp4">
                                    @else
                                    <source src="{{asset('public/videos/lively-07.mp4')}}" type="video/webm">
                                    <source src="{{asset('public/videos/lively-07.mp4')}}" type="video/mp4">
                                    @endif
                                </video>
                            </div>
                            <div class="slider-image simg" data-animation="fadeInRight" data-delay=".3s">
                                <img class="pcphn" src="{{asset('public/img/mockups/app-mockup-1.png')}}" alt="">
                                <img class="mobphn" src="{{asset('public/img/mockups/app-mockup-3.png')}}" alt="">
                            </div>


                        <!-- </div> -->
                    </div>
                </div>
            </div>
            
        </section>
        <!-- slider area end -->
        <!-- app features area start -->
        <section id="app-features-area" class="ptb-120">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 p-0 wow fadeIn" data-wow-duration="2s">
                        <div class="single-feature media">
                            <div class="feature-icon media-left">
                                <img src="{{asset('public/img/icons/app-feature-1.png')}}" alt="">
                            </div>
                            <div class="feature-details media-body">
                                <h5 class="text-uppercase">easy to used</h5>
                                <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod teporara incididunt ugt labore.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-0 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.2s">
                        <div class="single-feature media">
                            <div class="feature-icon media-left">
                                <img src="{{asset('public/img/icons/app-feature-2.png')}}" alt="">
                            </div>
                            <div class="feature-details media-body">
                                <h5 class="text-uppercase">Awesome design</h5>
                                <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod teporara incididunt ugt labore.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 p-0 wow fadeIn" data-wow-duration="2s" data-wow-delay="0.4s">
                        <div class="single-feature media">
                            <div class="feature-icon media-left">
                                <img src="{{asset('public/img/icons/app-feature-3.png')}}" alt="">
                            </div>
                            <div class="feature-details media-body">
                                <h5 class="text-uppercase">EASY TO CUSTOMIZE</h5>
                                <p>Lorem ipsum dolor sit amt, consectet adop adipisicing elit, sed do eiusmod teporara incididunt ugt labore.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- app features area end -->
        <!-- <button class="preview" onclick="pop()">Preview</button> -->
        <!-- pricing table area start -->
        
        <!-- footer area start -->
        <footer id="footer-area">
            <div class="container">
                <!-- <ul class="social list-inline text-center">
                    <li><a href="#"><i class="icofont icofont-social-google-plus"></i></a></li>
                    <li><a href="#"><i class="icofont icofont-social-google-plus"></i></a></li>
                    <li><a href="#"><i class="icofont icofont-social-twitter"></i></a></li>
                    <li><a href="#"><i class="icofont icofont-social-instagram"></i></a></li>
                </ul> -->
                <div class="copyright text-center">
                    <p onclick="pop();">Copyright @ 2020 <a href="#"  target="_blank">CBTECH</a> all right reserved.</p>
                </div>
            </div>
        </footer>
        <!-- footer area end -->
    </div>
    <!-- page wrapper end -->
    <!-- All Js files-->
    <script src= "{{asset('js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
            $("input[type='text'], name").on("keyup", function(){
                if($(this).val() != "" && $("textarea").val() != "" && $("input[name='name']").is(":checked") == true){
                    $("input[type='submit']").removeAttr("disabled");
                }
            });
    </script>

    
    <script src= "{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src= "{{asset('js/plugins.js')}}"></script>
    <!-- <script src="js/ajax-mail.js"></script> -->
    <script src= "{{asset('js/main.js')}}"></script>
        @if (session('successMessage'))
                {!!  session('successMessage') !!}
        @endif
</body>
</html>
