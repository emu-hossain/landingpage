<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;
use App\Campaign;
use App\Landingpage;
use Session;
use Redirect;
use GeoIP as GeoIP;

class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function emailSubmit(Request $request)
    {
        $data =geoip()->getLocation();
        // dd($request);
        $email = new Email();
        $email->purpose = $request->purpose;
        $email->name = $request->name;
        $email->email = $request->email;
        $email->save();

        $camp = new Campaign();
        $camp->name = $request->campname;
        $camp->save();

        $landingpage = new Landingpage();
        $landingpage->camp_id = $camp->id;
        $landingpage->name = $request->name;
        $landingpage->email = $request->email;
        $landingpage->save();
        $successMessage = '<script type="text/javascript">
            swal({
                  title: "Thanks for filling out your information!",
                  text: "We will contact you very soon!",
                  icon: "success",
                  button: "Close!",
                });
            </script>';
        session()->flash('successMessage',$successMessage);
        return back();
    }

    public function alldata(){
        $allemails = Email::paginate(8);
        // dd($allemails);
        return view('emailList')->with('allemails', $allemails);
    }

    public function userdetech(){
      dd('jhbh');
    }
}
