<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;
use Session;
use Redirect;
use DataTables;
use App\Campaign;
use App\Landingpage;

class AppController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index(Request $request){
        // dd("sdssd");
        // dd($request);
        if($request->ajax())
        {

            $data = Landingpage::where('landingpages.camp_id','!=','')
            ->Leftjoin('campaigns', 'landingpages.camp_id', '=', 'campaigns.id')
            ->select('landingpages.*', 'campaigns.name as campname')
            ->get();
            // $data = Landingpage::all();
            return DataTables::of($data)
                    ->make(true);
        }
        return view('appData');
    }
}
